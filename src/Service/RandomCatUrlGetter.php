<?php

namespace App\Service;

class RandomCatUrlGetter
{
    public function getUrl(): string
    {
        return '/images/404.jpg';
    }
}